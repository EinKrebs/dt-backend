all: build down migrate collectstatic up

build:
	docker-compose build

up:
	docker-compose up

run_detached:
	docker-compose up -d

down:
	docker-compose down

push:
	docker-compose push

pull:
	docker-compose pull

test:
	docker-compose run --entrypoint 'bash -c' server 'pipenv run pytest'

migrate:
	docker-compose run server migrate

makemigrations:
	python3 src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	docker-compose run server createsuperuser

collectstatic:
	docker-compose run server collectstatic --no-input

command:
	docker-compose run server ${c}

shell_command:
	docker-compose run --entrypoint 'bash -c' server '${c}'

shell:
	docker-compose run server shell

debug:
	docker-compose run server debug

#TODO: add dev

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	c='isort --check --diff .' make shell_command
	c='flake8 --config setup.cfg' make shell_command
	c='black --check --config pyproject.toml .' make shell_command
