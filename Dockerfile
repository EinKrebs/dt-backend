FROM python:3.10-slim-buster

RUN pip install pipenv==2022.1.8

WORKDIR ./code

ADD Pipfile Pipfile.lock ./
RUN pipenv install --system --deploy --ignore-pipfile
ENV PYTHONDONTWRITEBYTECODE=1

COPY setup.cfg .
COPY pyproject.toml .

COPY src .
COPY .env .

ENTRYPOINT ["python3.10", "manage.py"]
