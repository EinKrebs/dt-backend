# Generated by Django 4.0.3 on 2022-04-14 06:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0010_alter_account_currency_alter_account_money_amount_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="telegramuser",
            name="favorites",
            field=models.ManyToManyField(to="app.telegramuser"),
        ),
    ]
