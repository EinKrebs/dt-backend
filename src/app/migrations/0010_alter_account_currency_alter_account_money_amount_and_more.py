# Generated by Django 4.0.3 on 2022-03-25 14:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0009_account_money_amount"),
    ]

    operations = [
        migrations.AlterField(
            model_name="account",
            name="currency",
            field=models.CharField(default="", max_length=32),
        ),
        migrations.AlterField(
            model_name="account",
            name="money_amount",
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=17),
        ),
        migrations.AlterField(
            model_name="account",
            name="number",
            field=models.CharField(max_length=20, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name="card",
            name="money_amount",
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=17),
        ),
        migrations.AlterField(
            model_name="card",
            name="number",
            field=models.CharField(max_length=16, primary_key=True, serialize=False),
        ),
    ]
