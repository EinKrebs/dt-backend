# Generated by Django 4.0.4 on 2022-05-04 11:35

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("contenttypes", "0002_remove_content_type_name"),
        ("app", "0011_telegramuser_favorites"),
    ]

    operations = [
        migrations.CreateModel(
            name="Operation",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("source_number", models.CharField(max_length=20)),
                ("destination_number", models.CharField(max_length=20)),
                ("money_amount", models.DecimalField(decimal_places=2, max_digits=17)),
                (
                    "destination_type",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.RESTRICT,
                        related_name="operation_destination_type",
                        to="contenttypes.contenttype",
                    ),
                ),
                (
                    "recipient",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="operation_recipient",
                        to="app.telegramuser",
                    ),
                ),
                (
                    "sender",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="operation_sender",
                        to="app.telegramuser",
                    ),
                ),
                (
                    "source_type",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.RESTRICT,
                        related_name="operation_source_type",
                        to="contenttypes.contenttype",
                    ),
                ),
            ],
        ),
    ]
