from django.core.management import BaseCommand

from app.internal.bot import run_polling


class Command(BaseCommand):
    help = "Run Telegram bot in polling mode"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        run_polling()
