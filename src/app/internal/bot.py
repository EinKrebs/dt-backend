import logging

import telegram
from telegram.ext import CommandHandler, MessageHandler, Updater
from telegram.ext.filters import Filters

from app.internal.transport.bot import handlers
from config.settings import env


def run_polling():
    telegram_token = env("TELEGRAM_TOKEN")
    updater = Updater(token=telegram_token, use_context=True)
    dispatcher = updater.dispatcher

    logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.DEBUG)

    dispatcher.add_handler(CommandHandler("start", handlers.start))
    dispatcher.add_handler(CommandHandler("phone", handlers.phone))
    dispatcher.add_handler(CommandHandler("me", handlers.get_user_info))
    dispatcher.add_handler(CommandHandler("card_amount", handlers.get_card_amount))
    dispatcher.add_handler(CommandHandler("account_amount", handlers.get_account_amount))
    dispatcher.add_handler(MessageHandler(Filters.contact, handlers.contact_callback))
    dispatcher.add_handler(CommandHandler("favorites", handlers.list_favorites))
    send_handler = telegram.ext.ConversationHandler(
        entry_points=[CommandHandler("send", handlers.send)],
        states={
            handlers.SOURCE: [
                CommandHandler("cancel", handlers.cancel),
                telegram.ext.CallbackQueryHandler(handlers.source),
            ],
            handlers.NUMBER: [
                CommandHandler("cancel", handlers.cancel),
                MessageHandler(Filters.text, handlers.number),
            ],
            handlers.AMOUNT: [
                CommandHandler("cancel", handlers.cancel),
                MessageHandler(Filters.text, handlers.amount),
            ],
            handlers.RECEIVER_KIND: [
                telegram.ext.CallbackQueryHandler(handlers.receiver_kind),
                CommandHandler("cancel", handlers.cancel),
            ],
            handlers.RECEIVER: [
                CommandHandler("cancel", handlers.cancel),
                MessageHandler(Filters.text, handlers.receiver),
            ],
            handlers.USER_TO_SEND_TO: [
                CommandHandler("cancel", handlers.cancel),
                telegram.ext.CallbackQueryHandler(handlers.user_to_send_to),
            ],
            handlers.RECEIVER_ACCOUNT: [
                CommandHandler("cancel", handlers.cancel),
                telegram.ext.CallbackQueryHandler(handlers.receiver_account),
            ],
        },
        fallbacks=[MessageHandler(Filters.all, handlers.cancel)],
    )
    dispatcher.add_handler(send_handler)

    favorites_add_handler = telegram.ext.ConversationHandler(
        entry_points=[CommandHandler("add_to_favorites", handlers.add_to_favourites)],
        states={
            handlers.ADD_USERNAME: [
                CommandHandler("cancel", handlers.cancel),
                MessageHandler(Filters.text, handlers.add_username),
            ]
        },
        fallbacks=[MessageHandler(Filters.all, handlers.cancel)],
    )
    dispatcher.add_handler(favorites_add_handler)

    favorites_remove_handler = telegram.ext.ConversationHandler(
        entry_points=[CommandHandler("remove_from_favorites", handlers.remove_from_favorites)],
        states={
            handlers.REMOVE_USERNAME: [
                CommandHandler("cancel", handlers.cancel),
                telegram.ext.CallbackQueryHandler(handlers.remove_username),
            ]
        },
        fallbacks=[MessageHandler(Filters.all, handlers.cancel)],
    )
    dispatcher.add_handler(favorites_remove_handler)

    vault_history_handler = telegram.ext.ConversationHandler(
        entry_points=[CommandHandler("operations", handlers.get_operation_history)],
        states={
            handlers.HISTORY_TYPE: [
                CommandHandler("cancel", handlers.cancel),
                telegram.ext.CallbackQueryHandler(handlers.history_type),
            ],
            handlers.HISTORY_NUMBER: [
                CommandHandler("cancel", handlers.cancel),
                MessageHandler(Filters.all, handlers.history_number),
            ],
        },
        fallbacks=[MessageHandler(Filters.all, handlers.cancel)],
    )
    dispatcher.add_handler(vault_history_handler)

    dispatcher.add_handler(CommandHandler("related", handlers.related_users))
    dispatcher.add_handler(CommandHandler("login", handlers.login))

    updater.start_webhook(
        listen="0.0.0.0",
        port=8443,
        url_path=telegram_token,
        webhook_url=f"https://ein-krebs.backend22.2tapp.cc/{telegram_token}",
    )
    updater.idle()
