from rest_framework.permissions import BasePermission


class IsPhoneNumberSet(BasePermission):
    message = "No phone number is set"

    def has_permission(self, request, view):
        phone_number = request.user.phone_number
        return phone_number is not None and phone_number != ""
