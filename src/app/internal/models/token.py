from datetime import datetime, timedelta

import jwt
from django.db import models

from config.settings import env

SECRET_KEY = "SECRET_KEY"
USER_ID_KEY = "user_id"


class JWT(models.Model):
    jti = models.CharField(max_length=255, primary_key=True)
    user = models.ForeignKey("TelegramUser", null=False, on_delete=models.CASCADE, related_name="refresh_token")
    created_at = models.DateTimeField(auto_now_add=True)
    revoked = models.BooleanField(default=False)

    def encode(self) -> str:
        return jwt.encode(
            {"jti": self.jti, USER_ID_KEY: self.user.id, "exp": self.created_at + timedelta(days=30)},
            env(SECRET_KEY),
            algorithm="HS256",
        )

    def generate_access(self) -> str:
        return jwt.encode(
            {
                USER_ID_KEY: self.user.id,
                "exp": datetime.now() + timedelta(days=2),
            },
            env(SECRET_KEY),
            algorithm="HS256",
        )

    @classmethod
    def decode_jwt(cls, token: str) -> dict:
        return jwt.decode(token, env(SECRET_KEY), algorithms=["HS256"])
