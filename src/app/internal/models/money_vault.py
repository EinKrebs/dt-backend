from abc import ABC, abstractmethod
from decimal import Decimal

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from app.internal.models.operation import Operation
from app.internal.models.telegram_user import TelegramUser


class MoneyVault(models.Model):
    number = models.CharField(primary_key=True, max_length=20)
    money_amount = models.DecimalField(default=0.0, decimal_places=2, max_digits=17)
    source_operations = GenericRelation(
        Operation,
        object_id_field="source_number",
        content_type_field="source_type",
    )
    destination_operations = GenericRelation(
        Operation,
        object_id_field="destination_number",
        content_type_field="destination_type",
    )

    def check_amount(self, amount: Decimal):
        if amount < 0:
            raise InvalidMoneyAmount
        if amount > self.money_amount:
            raise self.get_not_enough_money_error()

    @abstractmethod
    def check_ownership(self, user: TelegramUser):
        pass

    @abstractmethod
    def get_owner(self) -> TelegramUser:
        pass

    @abstractmethod
    def to_string(self) -> str:
        pass

    @abstractmethod
    def get_currency(self) -> str:
        pass

    @staticmethod
    @abstractmethod
    def get_not_enough_money_error(self):
        pass

    class Meta:
        abstract = True


class VaultNotFoundError(Exception, ABC):
    @staticmethod
    @abstractmethod
    def get_description():
        pass


class VaultAccessDeniedError(Exception, ABC):
    @staticmethod
    @abstractmethod
    def get_description():
        pass


class NotEnoughMoneyError(Exception, ABC):
    @staticmethod
    @abstractmethod
    def get_description() -> str:
        pass


class InvalidMoneyAmount(Exception):
    @staticmethod
    def get_description() -> str:
        return "Invalid money amount"
