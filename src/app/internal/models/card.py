from django.db import models

from app.internal.models import money_vault
from app.internal.models.account import Account, AccountAccessDeniedError
from app.internal.models.telegram_user import TelegramUser


class Card(money_vault.MoneyVault):
    number = models.CharField(primary_key=True, max_length=16)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)

    def check_ownership(self, user: TelegramUser):
        try:
            self.account.check_ownership(user)
        except AccountAccessDeniedError:
            raise CardAccessDeniedError

    def get_owner(self) -> TelegramUser:
        return self.account.owner

    def get_currency(self) -> str:
        return self.account.currency

    def to_string(self) -> str:
        return f"Card {self.number}"

    @staticmethod
    def get_not_enough_money_error() -> money_vault.NotEnoughMoneyError:
        return NotEnoughMoneyError


class CardAccessDeniedError(money_vault.VaultAccessDeniedError):
    @staticmethod
    def get_description():
        return "Card access denied"


class CardNotFoundError(money_vault.VaultNotFoundError):
    @staticmethod
    def get_description():
        return "No such card"


class NotEnoughMoneyError(money_vault.NotEnoughMoneyError):
    @staticmethod
    def get_description() -> str:
        return "Недостаточно средств на карте"
