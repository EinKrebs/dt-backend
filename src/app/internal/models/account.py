import logging

from django.db import models

from app.internal.models import money_vault
from app.internal.models.telegram_user import TelegramUser


class Account(money_vault.MoneyVault):
    owner = models.ForeignKey(TelegramUser, on_delete=models.CASCADE)
    currency = models.CharField(default="", max_length=32)

    def check_ownership(self, user: TelegramUser):
        if self.owner.id != user.id:
            raise AccountAccessDeniedError

    def get_currency(self) -> str:
        return self.currency

    def get_owner(self) -> TelegramUser:
        return self.owner

    def to_string(self) -> str:
        return f"Account {self.number}"

    @staticmethod
    def get_not_enough_money_error() -> money_vault.NotEnoughMoneyError:
        return NotEnoughMoneyError


class AccountAccessDeniedError(money_vault.VaultAccessDeniedError):
    @staticmethod
    def get_description():
        return "Account access denied"


class AccountNotFoundError(money_vault.VaultNotFoundError):
    @staticmethod
    def get_description():
        return "No such account"


class NotEnoughMoneyError(money_vault.NotEnoughMoneyError):
    @staticmethod
    def get_description() -> str:
        return "Недостаточно средств на счёте"
