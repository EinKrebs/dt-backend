from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models


class Operation(models.Model):
    sender = models.ForeignKey("TelegramUser", on_delete=models.CASCADE, related_name="operation_sender")
    recipient = models.ForeignKey("TelegramUser", on_delete=models.CASCADE, related_name="operation_recipient")
    source_type = models.ForeignKey(ContentType, on_delete=models.RESTRICT, related_name="operation_source_type")
    source_number = models.CharField(max_length=20)
    source = GenericForeignKey("source_type", "source_number")
    destination_type = models.ForeignKey(
        ContentType, on_delete=models.RESTRICT, related_name="operation_destination_type"
    )
    destination_number = models.CharField(max_length=20)
    destination = GenericForeignKey("destination_type", "destination_number")
    money_amount = models.DecimalField(decimal_places=2, max_digits=17)

    def to_string(self):
        return (
            f"From: {self.sender.to_string_short()}, {self.source.to_string()}\n"
            f"To: {self.recipient.to_string_short()}, {self.destination.to_string()}\n"
            f"Money amount: {self.money_amount}"
        )
