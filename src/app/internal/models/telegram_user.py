from django.db import models


class TelegramUser(models.Model):
    id = models.IntegerField(primary_key=True)
    is_bot = models.BooleanField()
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    username = models.CharField(max_length=255, null=True, blank=True, unique=True)
    language_code = models.CharField(max_length=50, null=True, blank=True)
    phone_number = models.CharField(max_length=32, null=True, blank=True, unique=True)
    favorites = models.ManyToManyField("TelegramUser", blank=True)
    is_authenticated = models.BooleanField(null=False, default=False)

    def to_string(self) -> str:
        res = f"id: {self.id}\n" f"is bot: {self.is_bot}\n" f'first name: "{self.first_name}"\n'
        extra = []
        if self.last_name is not None:
            extra.append(("last name", self.last_name))
        if self.username is not None:
            extra.append(("username", self.username))
        if self.language_code is not None:
            extra.append(("language code", self.language_code))
        if self.phone_number is not None:
            extra.append(("phone number", self.phone_number))
        res += "\n".join(map(lambda pair: f'{pair[0]}: "{pair[1]}"', extra))
        return res

    def to_string_short(self) -> str:
        if self.username is not None:
            return f"@{self.username}"
        if self.last_name is not None:
            return f"{self.first_name} {self.last_name}"
        return str(self.first_name)


class UserNotFoundError(Exception):
    pass


class PhoneNumberNotProvidedError(Exception):
    pass
