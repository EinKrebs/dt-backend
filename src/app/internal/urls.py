from django.urls import path

from app.internal.transport.rest import handlers

urlpatterns = [path("me/", handlers.get_me), path("update_tokens/", handlers.update_tokens)]
