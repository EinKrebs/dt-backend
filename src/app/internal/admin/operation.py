from django.contrib import admin

from app.internal.models.operation import Operation


@admin.register(Operation)
class OperationAdmin(admin.ModelAdmin):
    pass
