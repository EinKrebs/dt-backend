from django.contrib import admin

from app.internal.models.token import JWT


@admin.register(JWT)
class JWTAdmin(admin.ModelAdmin):
    pass
