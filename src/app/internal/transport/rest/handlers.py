import json
import logging

import django.http as http
import jwt
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated

from app.internal.authentication.backends import JWTAuthentication
from app.internal.models.token import JWT
from app.internal.permission.backends import IsPhoneNumberSet
from app.internal.services.token_service import generate_tokens, get_token, revoke_user_tokens


@api_view(["GET"])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated, IsPhoneNumberSet])
def get_me(request):
    return http.HttpResponse(request.user.to_string(), content_type="text/plain; charset=utf-8")


@api_view(["POST"])
def update_tokens(request):
    body = request.body.decode("utf-8")
    try:
        data = json.loads(body)
    except json.JSONDecodeError:
        return http.HttpResponseBadRequest("invalid JSON data")
    if "access_token" not in data or "refresh_token" not in data:
        return http.HttpResponseBadRequest("invalid JSON data")
    try:
        _ = JWT.decode_jwt(data["access_token"])
    except jwt.ExpiredSignatureError:
        pass
    except jwt.PyJWTError as e:
        return http.HttpResponseBadRequest(f"Could not decode tokens: {e}")
    logging.log(logging.WARN, "Access decoded")
    try:
        refresh_token_payload = JWT.decode_jwt(data["refresh_token"])
    except jwt.ExpiredSignatureError:
        return http.HttpResponseBadRequest("Refresh token expired, please re-authenticate")
    except jwt.PyJWTError as e:
        return http.HttpResponseBadRequest(f"Could not decode tokens: {e}")
    logging.log(logging.WARN, f"Token paylaod: {refresh_token_payload.items()}")
    if "jti" not in refresh_token_payload:
        return http.HttpResponseBadRequest("Invalid token payload")
    refresh_token = get_token(refresh_token_payload["jti"])
    if refresh_token is None:
        return http.HttpResponseBadRequest("No such token")
    if refresh_token.revoked:
        return http.HttpResponseBadRequest("Refresh token revoked, please re-authenticate")
    user = refresh_token.user
    revoke_user_tokens(user)
    new_refresh, new_access = generate_tokens(user)
    return http.JsonResponse(
        {
            "access_token": new_access,
            "refresh_token": new_refresh,
        }
    )
