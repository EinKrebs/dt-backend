import logging
from decimal import Decimal, InvalidOperation

import telegram
from telegram import Update
from telegram.ext import CallbackContext

import app.internal.models.account as account_model
import app.internal.models.card as card_model
import app.internal.models.money_vault as vault_model
from app.internal.models.telegram_user import PhoneNumberNotProvidedError, TelegramUser, UserNotFoundError
from app.internal.services import account_service, card_service, telegram_user_service, vault_service
from app.internal.services.telegram_user_service import add_phone, add_user, get_user, get_user_by_name
from app.internal.services.token_service import generate_tokens, revoke_user_tokens

USE = "Couldn't get user data"

START_REPLY = "Hello, now send me a command to start!"
NUMBER_SHARE_REQUEST = "Would you share your phone number?"
PHONE_NUMBER_IS_SAVED = "Phone number is saved"
NO_CARD_NUMBER = "No card number provided"
NO_ACCOUNT_NUMBER = "No account number provided"
NO_PHONE_NUMBER = "No phone number is set"
USER_NOT_FOUND = "User not found"
NO_USER_DATA = "Couldn't get user data"
TRY_AGAIN = ", try again"

SEND_FROM_CARD = "send_money_from_card"
SEND_FROM_ACCOUNT = "send_money_from_account"
SEND_TO_USER = "send_money_to_user"
SEND_TO_ACCOUNT = "send_money_to_account"
SEND_TO_CARD = "send_money_to_card"
HISTORY_CARD = "card"
HISTORY_ACCOUNT = "account"
(
    SOURCE,
    NUMBER,
    AMOUNT,
    RECEIVER_KIND,
    USER_TO_SEND_TO,
    RECEIVER,
    RECEIVER_ACCOUNT,
    ADD_USERNAME,
    REMOVE_USERNAME,
    HISTORY_TYPE,
    HISTORY_NUMBER,
) = range(11)


def start(update: Update, context: CallbackContext):
    user = update.effective_user
    if user is None:
        context.bot.send_message(chat_id=update.effective_chat.id, text=NO_USER_DATA)
        return
    user_model = TelegramUser(
        id=user.id,
        is_bot=user.is_bot,
        first_name=user.first_name,
        last_name=user.last_name,
        username=user.username,
        language_code=user.language_code,
    )
    add_user(user_model)
    context.bot.send_message(chat_id=update.effective_chat.id, text=START_REPLY)


def phone(update: Update, context: CallbackContext):
    contact_button = telegram.KeyboardButton("Share contact", request_contact=True)
    reply_markup = telegram.ReplyKeyboardMarkup([[contact_button]], one_time_keyboard=True)
    context.bot.send_message(chat_id=update.effective_chat.id, text=NUMBER_SHARE_REQUEST, reply_markup=reply_markup)


def contact_callback(update: Update, context: CallbackContext):
    contact = update.effective_message.contact
    user_id = update.effective_user.id
    try:
        add_phone(user_id, contact.phone_number)
    except UserNotFoundError:
        context.bot.send_message(chat_id=update.effective_chat.id, text=USER_NOT_FOUND)
    context.bot.send_message(chat_id=update.effective_chat.id, text=PHONE_NUMBER_IS_SAVED)


def get_user_info(update: Update, context: CallbackContext):
    try:
        user_data = get_authenticated_user(update.effective_user.id, update, context)
    except PhoneNumberNotProvidedError or UserNotFoundError:
        return
    context.bot.send_message(chat_id=update.effective_chat.id, text=f"Your data is:\n{user_data.to_string()}")


def get_card_amount(update: Update, context: CallbackContext):
    if len(context.args) == 0:
        context.bot.send_message(chat_id=update.effective_chat.id, text=NO_CARD_NUMBER)
        return
    try:
        user_data = get_authenticated_user(update.effective_user.id, update, context)
    except PhoneNumberNotProvidedError or UserNotFoundError:
        return
    vault_number = context.args[0]
    card = card_service.get_card(vault_number)
    if card is None:
        context.bot.send_message(chat_id=update.effective_chat.id, text=card_model.CardNotFoundError.get_description())
        return
    try:
        card.check_ownership(user_data)
        currency = card.account.currency
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Money amount on card {vault_number} is {card.money_amount}"
            f"{('' if currency == '' else ' ') + currency}",
        )
    except card_model.CardAccessDeniedError as e:
        context.bot.send_message(chat_id=update.effective_chat.id, text=e.get_description())


def get_account_amount(update: Update, context: CallbackContext):
    if len(context.args) == 0:
        context.bot.send_message(chat_id=update.effective_chat.id, text=NO_ACCOUNT_NUMBER)
        return
    try:
        user_data = get_authenticated_user(update.effective_user.id, update, context)
    except PhoneNumberNotProvidedError or UserNotFoundError:
        return
    account_number = context.args[0]
    account = account_service.get_account(account_number)
    if account is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=account_model.AccountNotFoundError.get_description()
        )
        return
    try:
        account.check_ownership(user_data)
    except account_model.AccountAccessDeniedError as e:
        context.bot.send_message(chat_id=update.effective_chat.id, text=e.get_description())
        return
    currency = account.currency
    account_cards = card_service.get_cards_of_account(account)
    logging.log(logging.INFO, account_cards)
    card_total_amount = sum(map(lambda card: card.money_amount, account_cards))
    currency_suffix = ("" if currency == "" else " ") + currency
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Money amount on account {account_number} is {account.money_amount}"
        f"{currency_suffix}.\n"
        f"Money amount of all linked cards is {card_total_amount}"
        f"{currency_suffix}",
    )


def send(update: Update, context: CallbackContext) -> int:
    try:
        _ = get_authenticated_user(update.effective_user.id, update, context)
    except PhoneNumberNotProvidedError or UserNotFoundError:
        return telegram.ext.ConversationHandler.END
    buttons = [
        telegram.InlineKeyboardButton("Card", callback_data=SEND_FROM_CARD),
        telegram.InlineKeyboardButton("Account", callback_data=SEND_FROM_ACCOUNT),
    ]
    update.message.reply_text(
        "Where to send money from?", reply_markup=telegram.InlineKeyboardMarkup([[x] for x in buttons])
    )
    return SOURCE


def source(update: Update, context: CallbackContext) -> int:
    query = update.callback_query
    query.answer()

    reply = query.data
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=f"Enter {'card' if reply == SEND_FROM_CARD else 'account'} number"
    )
    context.user_data["source"] = reply
    return NUMBER


def number(update: Update, context: CallbackContext) -> int:
    number = update.message.text
    source = context.user_data["source"]
    user_id = update.message.from_user.id
    try:
        user_data = get_authenticated_user(user_id, update, context)
    except UserNotFoundError or PhoneNumberNotProvidedError:
        return telegram.ext.ConversationHandler.END

    if source == SEND_FROM_CARD:
        vault = card_service.get_card(number)
        if vault is None:
            update.message.reply_text(card_model.CardNotFoundError.get_description() + TRY_AGAIN)
            return NUMBER
    elif source == SEND_FROM_ACCOUNT:
        vault = account_service.get_account(number)
        if vault is None:
            update.message.reply_text(account_model.AccountNotFoundError.get_description() + TRY_AGAIN)
            return NUMBER
    else:
        update.message.reply("Unknown money source")
        return telegram.ext.ConversationHandler.END

    try:
        vault.check_ownership(user_data)
    except vault_model.VaultAccessDeniedError as e:
        update.message.reply_text(e.get_description() + TRY_AGAIN)
        return NUMBER

    context.user_data["send_from"] = vault
    update.message.reply_text(
        f"You have {vault.money_amount} {vault.get_currency()}, " "how much money do you want to send?"
    )
    return AMOUNT


def amount(update: Update, context: CallbackContext) -> int:
    try:
        amount = Decimal(value=update.message.text)
    except InvalidOperation:
        update.message.reply_text(vault_model.InvalidMoneyAmount.get_description() + TRY_AGAIN)
        return AMOUNT
    try:
        context.user_data["send_from"].check_amount(amount)
    except vault_model.NotEnoughMoneyError:
        update.message.reply_text("Too much money, try again")
        return AMOUNT
    except vault_model.InvalidMoneyAmount as e:
        update.message.reply_text(e.get_description() + TRY_AGAIN)
        return AMOUNT
    context.user_data["amount"] = amount
    reply_markup = telegram.InlineKeyboardMarkup(
        [
            [
                telegram.InlineKeyboardButton("To card", callback_data=SEND_TO_CARD),
            ],
            [
                telegram.InlineKeyboardButton("To account", callback_data=SEND_TO_ACCOUNT),
            ],
            [
                telegram.InlineKeyboardButton("To user", callback_data=SEND_TO_USER),
            ],
        ]
    )
    update.message.reply_text("Where to send money?", reply_markup=reply_markup)
    return RECEIVER_KIND


def receiver_kind(update: Update, context: CallbackContext) -> int:
    query = update.callback_query
    query.answer()

    reply = query.data
    context.user_data["receiver_kind"] = reply
    if reply == SEND_TO_ACCOUNT:
        text = "Enter account number"
    elif reply == SEND_TO_CARD:
        text = "Enter card number"
    elif reply == SEND_TO_USER:
        return show_users_to_send_to(update, context)
    context.bot.send_message(chat_id=update.effective_chat.id, text=text)
    return RECEIVER


def show_users_to_send_to(update: Update, context: CallbackContext) -> int:
    user = get_authenticated_user(update.effective_user.id, update, context)
    favorites = user.favorites.all()
    if len(favorites) == 0:
        context.bot.send_message(chat_id=update.effective_chat.id, text="No users to send to")
        return telegram.ext.ConversationHandler.END
    reply_markup = telegram.InlineKeyboardMarkup(
        [
            [telegram.InlineKeyboardButton(telegram_user.username, callback_data=telegram_user.username)]
            for telegram_user in favorites
        ]
    )
    context.bot.send_message(chat_id=update.effective_chat.id, text="Whom to send to?", reply_markup=reply_markup)
    return USER_TO_SEND_TO


def user_to_send_to(update: Update, context: CallbackContext) -> int:
    query = update.callback_query
    query.answer()

    username = query.data
    receiver_user = get_user_by_name(username)
    if receiver_user is None:
        update.message.reply_text(USER_NOT_FOUND)
        return telegram.ext.ConversationHandler.END
    accounts = account_service.get_users_accounts(receiver_user)
    reply_markup = telegram.InlineKeyboardMarkup(
        [[telegram.InlineKeyboardButton(account.number, callback_data=account.number)] for account in accounts]
    )
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="For which account do you want to send your money?",
        reply_markup=reply_markup,
    )
    return RECEIVER_ACCOUNT


def receiver(update: Update, context: CallbackContext) -> int:
    receiver_kind = context.user_data["receiver_kind"]
    if "send_to" in context.user_data:
        send_to = context.user_data["send_to"]
    elif receiver_kind == SEND_TO_ACCOUNT:
        account_number = update.message.text
        account = account_service.get_account(account_number)
        if account is None:
            update.message.reply_text(account_model.AccountNotFoundError.get_description())
            return telegram.ext.ConversationHandler.END
        send_to = account
    elif receiver_kind == SEND_TO_CARD:
        card_number = update.message.text
        card = card_service.get_card(card_number)
        if card is None:
            update.message.reply_text(card_model.CardNotFoundError.get_description())
            return telegram.ext.ConversationHandler.END
        send_to = card
    vault_service.send_money(context.user_data["send_from"], send_to, context.user_data["amount"])
    context.bot.send_message(chat_id=update.effective_chat.id, text="Success!")
    return telegram.ext.ConversationHandler.END


def receiver_account(update: Update, context: CallbackContext) -> int:
    query = update.callback_query
    query.answer()

    account_number = query.data
    account = account_service.get_account(account_number)
    if account is None:
        update.message.reply_text(account_model.AccountNotFoundError.get_description())
        return telegram.ext.ConversationHandler.END
    context.user_data["send_to"] = account
    return receiver(update, context)


def add_to_favourites(update: Update, context: CallbackContext) -> int:
    try:
        _ = get_authenticated_user(update.effective_user.id, update, context)
    except UserNotFoundError or PhoneNumberNotProvidedError:
        return telegram.ext.ConversationHandler.END
    context.bot.send_message(chat_id=update.effective_chat.id, text="Enter username to add to your favorites")
    return ADD_USERNAME


def add_username(update: Update, context: CallbackContext) -> int:
    user = get_authenticated_user(update.effective_user.id, update, context)
    username = update.message.text
    user_to_add = telegram_user_service.get_user_by_name(username)
    if user_to_add is None:
        update.message.reply_text(USER_NOT_FOUND + TRY_AGAIN)
        return ADD_USERNAME
    telegram_user_service.add_to_favorites(user, user_to_add)
    update.message.reply_text("Success!")
    return telegram.ext.ConversationHandler.END


def remove_from_favorites(update: Update, context: CallbackContext) -> int:
    try:
        user = get_authenticated_user(update.effective_user.id, update, context)
    except UserNotFoundError or PhoneNumberNotProvidedError:
        return telegram.ext.ConversationHandler.END
    favorites = user.favorites.all()
    if len(favorites) == 0:
        context.bot.send_message(chat_id=update.effective_chat.id, text="No one to delete!")
        return telegram.ext.ConversationHandler.END
    reply_markup = telegram.InlineKeyboardMarkup(
        [[telegram.InlineKeyboardButton(favorite.username, callback_data=favorite.username)] for favorite in favorites]
    )
    context.bot.send_message(chat_id=update.effective_chat.id, text="Whom to delete?", reply_markup=reply_markup)
    return REMOVE_USERNAME


def remove_username(update: Update, context: CallbackContext) -> int:
    user = get_authenticated_user(update.effective_user.id, update, context)
    query = update.callback_query
    query.answer()

    username = query.data
    user_to_remove = telegram_user_service.get_user_by_name(username)
    if user_to_remove is None:
        update.message.reply_text(USER_NOT_FOUND + TRY_AGAIN)
        return REMOVE_USERNAME
    telegram_user_service.remove_from_favorites(user, user_to_remove)
    context.bot.send_message(chat_id=update.effective_chat.id, text="Success!")
    return telegram.ext.ConversationHandler.END


def list_favorites(update: Update, context: CallbackContext) -> None:
    try:
        user = get_authenticated_user(update.effective_user.id, update, context)
    except UserNotFoundError or PhoneNumberNotProvidedError:
        return
    favorites = user.favorites.all()
    if len(favorites) == 0:
        context.bot.send_message(chat_id=update.effective_chat.id, text="You have no favorite users!")
        return
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Your favorite users:\n" + "\n".join(map(lambda x: f" - {x.username}", favorites)),
    )


def get_operation_history(update: Update, context: CallbackContext) -> int:
    try:
        _ = get_authenticated_user(update.effective_user.id, update, context)
    except UserNotFoundError or PhoneNumberNotProvidedError:
        return telegram.ext.ConversationHandler.END
    reply_markup = telegram.InlineKeyboardMarkup(
        [
            [
                telegram.InlineKeyboardButton("Card", callback_data=HISTORY_CARD),
            ],
            [
                telegram.InlineKeyboardButton("Account", callback_data=HISTORY_ACCOUNT),
            ],
        ]
    )
    update.message.reply_text("What kind of vault?", reply_markup=reply_markup)
    return HISTORY_TYPE


def history_type(update: Update, context: CallbackContext) -> int:
    query = update.callback_query
    query.answer()

    vault_type = query.data
    context.user_data["vault_type"] = vault_type
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Enter the number of {'card' if vault_type == HISTORY_CARD else 'account'}",
    )
    return HISTORY_NUMBER


def history_number(update: Update, context: CallbackContext) -> int:
    vault_number = update.message.text
    vault_type = context.user_data["vault_type"]
    if vault_type == HISTORY_CARD:
        vault = card_service.get_card(vault_number)
        if vault is None:
            update.message.reply_text(card_model.CardNotFoundError.get_description())
            return telegram.ext.ConversationHandler.END
    elif vault_type == HISTORY_ACCOUNT:
        vault = account_service.get_account(vault_number)
        if vault is None:
            update.message.reply_text(account_model.AccountNotFoundError.get_description())
            return telegram.ext.ConversationHandler.END
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="Unknown vault type!")
        return telegram.ext.ConversationHandler.END

    user = get_authenticated_user(update.effective_user.id, update, context)
    try:
        vault.check_ownership(user)
    except vault_model.VaultAccessDeniedError as e:
        update.message.reply_text(e.get_description())
        return telegram.ext.ConversationHandler.END

    operations = vault_service.get_operations(vault)
    if len(operations) == 0:
        update.message.reply_text("No operations")
    else:
        text = f"{vault.to_string()} operations:\n" + "\n\n".join(
            map(lambda operation: operation.to_string(), operations)
        )
        update.message.reply_text(text)
    return telegram.ext.ConversationHandler.END


def related_users(update: Update, context: CallbackContext) -> int:
    try:
        user = get_authenticated_user(update.effective_user.id, update, context)
    except UserNotFoundError or PhoneNumberNotProvidedError:
        return telegram.ext.ConversationHandler.END
    senders, recipients = telegram_user_service.get_related_usernames(user)
    if len(senders) == 0 and len(recipients) == 0:
        update.message.reply_text("No related users!")
        return telegram.ext.ConversationHandler.END
    recipients_text = ("You sent money to:\n" + "\n".join(recipients)) if len(recipients) > 0 else ""
    senders_text = ("Sent money to you:\n" + "\n".join(senders)) if len(senders) > 0 else ""
    update.message.reply_text(recipients_text + ("\n\n" if len(recipients) > 0 else "") + senders_text)
    return telegram.ext.ConversationHandler.END


def login(update: Update, context: CallbackContext) -> None:
    try:
        user = get_authenticated_user(update.effective_user.id, update, context)
    except UserNotFoundError or PhoneNumberNotProvidedError:
        return telegram.ext.ConversationHandler.END
    revoke_user_tokens(user)
    refresh_token, access_token = generate_tokens(user)
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Your access token: {access_token}\n" f"your refresh token: {refresh_token}",
    )


def cancel(update: Update, context: CallbackContext) -> int:
    update.message.reply_text("Ok, operation cancelled")
    return telegram.ext.ConversationHandler.END


def get_authenticated_user(user_id: int, update: Update, context: CallbackContext) -> TelegramUser:
    user_data = get_user(user_id)
    if user_data is None:
        context.bot.send_message(chat_id=update.effective_chat.id, text=USER_NOT_FOUND)
        raise UserNotFoundError
    if user_data.phone_number is None:
        context.bot.send_message(chat_id=update.effective_chat.id, text=NO_PHONE_NUMBER)
        raise PhoneNumberNotProvidedError
    return user_data
