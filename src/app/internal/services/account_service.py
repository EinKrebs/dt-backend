from app.internal.models.account import Account, AccountAccessDeniedError
from app.internal.models.telegram_user import TelegramUser


def get_account(account_number: str) -> Account:
    return Account.objects.filter(number=account_number).select_related("owner").first()


def get_users_accounts(user: TelegramUser) -> list:
    return Account.objects.filter(owner=user).all()
