from django.db.models import Prefetch

from app.internal.models.account import Account
from app.internal.models.card import Card


def get_card(card_number: str) -> Card:
    return (
        Card.objects.filter(number=card_number)
        .prefetch_related(Prefetch("account", queryset=Account.objects.select_related("owner")))
        .first()
    )


def get_cards_of_account(account: Account):
    cards = Card.objects.filter(account=account)
    return cards
