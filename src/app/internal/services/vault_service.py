from decimal import Decimal
from typing import List

import django

from app.internal.models import money_vault
from app.internal.models.money_vault import MoneyVault
from app.internal.models.operation import Operation


def send_money(send_from: money_vault.MoneyVault, send_to: money_vault.MoneyVault, amount: Decimal) -> None:
    operation = Operation(
        sender=send_from.get_owner(),
        recipient=send_to.get_owner(),
        source=send_from,
        destination=send_to,
        money_amount=amount,
    )
    with django.db.transaction.atomic():
        send_from.check_amount(amount)
        send_from.money_amount -= amount
        send_from.save()
        send_to.money_amount += amount
        send_to.save()
        operation.save()


def get_operations(vault: MoneyVault) -> List[Operation]:
    return list(vault.source_operations.all()) + list(vault.destination_operations.all())
