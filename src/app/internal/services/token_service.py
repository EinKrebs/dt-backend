import uuid
from datetime import datetime
from typing import Tuple

from django.db.models import Model

from app.internal.models.telegram_user import TelegramUser
from app.internal.models.token import JWT


def generate_tokens(user: TelegramUser) -> Tuple[str, str]:
    refresh_token = JWT(jti=str(uuid.uuid4()), user=user, created_at=datetime.now())
    refresh_token.save()
    access_token = refresh_token.generate_access()
    return refresh_token.encode(), access_token


def revoke_user_tokens(user: TelegramUser) -> None:
    JWT.objects.filter(user=user, revoked=False).update(revoked=True)


def get_token(jti: str) -> JWT | None:
    try:
        return JWT.objects.get(jti=jti)
    except Model.DoesNotExist:
        return None
