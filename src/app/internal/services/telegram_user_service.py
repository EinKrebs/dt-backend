from typing import List, Tuple

from django.db.models import Q

from app.internal.models.operation import Operation
from app.internal.models.telegram_user import TelegramUser, UserNotFoundError


# TODO: broken + add unit tests
def add_user(user: TelegramUser):
    TelegramUser.objects.get_or_create(
        id=user.id,
        defaults={
            "first_name": user.first_name,
            "last_name": user.last_name,
            "is_bot": user.is_bot,
            "username": user.username,
            "language_code": user.language_code,
        },
    )


def add_phone(user_id: int, phone_number: str):
    user = get_user(user_id)
    if user is None:
        raise UserNotFoundError
    user.phone_number = phone_number
    user.save()


def get_user(user_id: int) -> TelegramUser:
    user = TelegramUser.objects.filter(id=user_id).first()
    return user


def get_users(ids: List[int]) -> List[TelegramUser]:
    return list(TelegramUser.objects.filter(id__in=ids))


def get_user_by_name(username: str) -> TelegramUser:
    user = TelegramUser.objects.filter(username=username).first()
    return user


def add_to_favorites(user: TelegramUser, user_to_add: TelegramUser) -> None:
    user.favorites.add(user_to_add)
    user.save()


def remove_from_favorites(user: TelegramUser, user_to_remove: TelegramUser) -> None:
    user.favorites.remove(user_to_remove)
    user.save()


def get_related_usernames(user: TelegramUser) -> Tuple[List[str], List[str]]:
    recipients = (
        Operation.objects.filter(Q(sender=user) & Q(recipient__username__isnull=False))
        .distinct("recipient__username")
        .values_list("recipient__username", flat=True)
    )
    senders = (
        Operation.objects.filter(Q(recipient=user) & Q(sender__username__isnull=False))
        .distinct("sender__username")
        .values_list("sender__username", flat=True)
    )
    return list(senders), list(recipients)
