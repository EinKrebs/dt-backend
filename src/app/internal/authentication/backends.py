import logging

import jwt
from django.conf import settings
from rest_framework import authentication, exceptions

from app.internal.services.telegram_user_service import get_user


class JWTAuthentication(authentication.BaseAuthentication):
    authentication_header_prefix = "Token"

    def authenticate(self, request):
        request.user = None

        auth_header = authentication.get_authorization_header(request).split()
        auth_header_prefix = self.authentication_header_prefix.lower()

        if not auth_header:
            return None

        if len(auth_header) == 1:
            return None

        elif len(auth_header) > 2:
            return None

        prefix = auth_header[0].decode("utf-8")
        token = auth_header[1].decode("utf-8")

        if prefix.lower() != auth_header_prefix:
            return None

        return self._authenticate_credentials(request, token)

    @staticmethod
    def _authenticate_credentials(request, token):
        try:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms="HS256")
        except jwt.ExpiredSignatureError as e:
            logging.log(logging.ERROR, e)
            raise exceptions.AuthenticationFailed("Authentication failed: access token expired")
        except jwt.PyJWTError as e:
            logging.log(logging.ERROR, e)
            raise exceptions.AuthenticationFailed("Authentication failed: cannot decode token")

        user = get_user(payload["user_id"])
        if user is None:
            raise exceptions.AuthenticationFailed("Authentication failed: no such user")

        user.is_authenticated = True
        user.save()
        return user, token
