from django.contrib import admin

from app.internal.admin.account import AccountAdmin
from app.internal.admin.admin_user import AdminUserAdmin
from app.internal.admin.card import CardAdmin
from app.internal.admin.operation import OperationAdmin
from app.internal.admin.telegram_user import TelegramUserAdmin
from app.internal.admin.token import JWTAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
