import contextlib

import pytest

from app.internal.models.account import Account
from app.internal.models.card import Card, CardAccessDeniedError, NotEnoughMoneyError
from app.internal.models.telegram_user import TelegramUser


class TestCard:
    OWNER_ID = "329714238"

    def test_no_money(self):
        with pytest.raises(NotEnoughMoneyError):
            raise Card().get_not_enough_money_error()

    @pytest.mark.parametrize(
        "user_to_check,expectation",
        [
            (TelegramUser(id=OWNER_ID), contextlib.nullcontext()),
            (TelegramUser(id="123481234"), pytest.raises(CardAccessDeniedError)),
            (TelegramUser(id=OWNER_ID, username="ab"), contextlib.nullcontext()),
        ],
    )
    def test_check_ownership(self, user_to_check, expectation):
        user = TelegramUser(id=self.OWNER_ID)
        card = Card(account=Account(owner=user))
        with expectation:
            card.check_ownership(user_to_check)
