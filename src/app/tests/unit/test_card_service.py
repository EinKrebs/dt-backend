import pytest

from app.internal.models.account import Account
from app.internal.models.card import Card
from app.internal.services.card_service import get_card, get_cards_of_account

pytestmark = pytest.mark.django_db


@pytest.mark.django_db
class TestCardService:
    pytestmark = pytest.mark.django_db

    @pytest.mark.parametrize(
        "number, expected",
        [
            ("2", Card(number="2")),
            ("11", None),
        ],
    )
    def test_get_card(self, number, expected, vaults_fixture):
        card = get_card(number)
        assert card == expected

    @pytest.mark.parametrize(
        "account, expected",
        [
            (Account(number="1"), []),
            (Account(number="2"), [Card(number="1")]),
        ],
    )
    def test_get_cards_of_account(self, account, expected, multiple_accounts_fixture):
        cards = list(get_cards_of_account(account))
        assert cards == expected
