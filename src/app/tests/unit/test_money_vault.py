import contextlib
from decimal import Decimal
from os import access

import pytest

from app.internal.models.account import Account
from app.internal.models.money_vault import InvalidMoneyAmount, MoneyVault, NotEnoughMoneyError


class TestMoneyVault:
    @pytest.mark.parametrize(
        "amount,expectation",
        [
            (Decimal(0.1), contextlib.nullcontext()),
            (Decimal(0), contextlib.nullcontext()),
            (Decimal(1), contextlib.nullcontext()),
            (Decimal(-1), pytest.raises(InvalidMoneyAmount)),
            (Decimal(1.01), pytest.raises(NotEnoughMoneyError)),
        ],
    )
    def test_check_amount(self, amount, expectation):
        vault = Account(money_amount=Decimal(1))
        assert isinstance(vault, MoneyVault)
        with expectation:
            vault.check_amount(amount)
