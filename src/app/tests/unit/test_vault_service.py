from decimal import Decimal
from typing import List

import pytest

from app.internal.models.money_vault import InvalidMoneyAmount, MoneyVault, NotEnoughMoneyError
from app.internal.services.vault_service import get_operations, send_money

pytestmark = pytest.mark.django_db


@pytest.mark.django_db
class TestVaultService:
    pytestmark = pytest.mark.django_db

    @pytest.mark.parametrize(
        "send_from_index, send_to_index, amount",
        [
            (0, 1, Decimal(10)),
            (1, 0, Decimal(10)),
        ],
    )
    def test_send_money(self, send_from_index: int, amount, send_to_index: int, vaults_fixture: List[MoneyVault]):
        send_from = vaults_fixture[send_from_index]
        send_to = vaults_fixture[send_to_index]
        init_from = send_from.money_amount
        init_to = send_to.money_amount
        send_money(send_from, send_to, amount)
        assert send_from.money_amount == init_from - amount
        assert send_to.money_amount == init_to + amount

    @pytest.mark.parametrize(
        "amount, expectation",
        [
            (Decimal(10000), pytest.raises(NotEnoughMoneyError)),
            (Decimal(-10), pytest.raises(InvalidMoneyAmount)),
        ],
    )
    def test_send_money_errors(self, amount: Decimal, expectation, vaults_fixture: List[MoneyVault]):
        send_from = vaults_fixture[0]
        send_to = vaults_fixture[1]
        init_from = send_from.money_amount
        init_to = send_to.money_amount
        with expectation:
            send_money(send_from, send_to, amount)
        assert send_from.money_amount == init_from
        assert send_to.money_amount == init_to

    def test_get_operations(self, users_with_operation_fixture):
        _, accounts, operation = users_with_operation_fixture
        assert get_operations(accounts[0]) == [operation]
        assert get_operations(accounts[1]) == [operation]
        assert get_operations(accounts[2]) == []
