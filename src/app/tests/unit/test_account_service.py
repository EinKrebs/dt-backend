from decimal import Decimal

import pytest

from app.internal.models.account import Account
from app.internal.models.telegram_user import TelegramUser
from app.internal.services import account_service

pytestmark = pytest.mark.django_db


@pytest.mark.django_db
class TestAccountService:
    pytestmark = pytest.mark.django_db

    @pytest.mark.parametrize(
        "number, compare_with",
        [
            ("1", Account(number="1")),
            ("0", None),
        ],
    )
    def test_get_account(self, number, compare_with, vaults_fixture):
        account = account_service.get_account(number)
        assert account == compare_with

    @pytest.mark.parametrize(
        "user, result",
        [
            (
                TelegramUser(id=1),
                [
                    Account(number="1"),
                    Account(number="2"),
                ],
            ),
            (
                TelegramUser(id=2),
                [],
            ),
        ],
    )
    def test_get_users_accounts(self, user, result, multiple_accounts_fixture):
        accounts = list(account_service.get_users_accounts(user))
        assert accounts == result
