from decimal import Decimal

import pytest

from app.internal.models.account import Account
from app.internal.models.card import Card
from app.internal.models.operation import Operation
from app.internal.models.telegram_user import TelegramUser


@pytest.fixture(scope="function")
def user_fixture():
    return TelegramUser.objects.create(id=1, first_name="A", username="a", is_bot=False)


@pytest.fixture(scope="function")
def vaults_fixture():
    user = TelegramUser.objects.create(id=1, first_name="A", username="a", is_bot=False)
    account = Account.objects.create(number="1", money_amount=Decimal(10), owner=user)
    card = Card.objects.create(number="2", money_amount=Decimal(10), account=account)
    return account, card


@pytest.fixture(scope="function")
def multiple_accounts_fixture():
    first_user = TelegramUser.objects.create(id=1, first_name="A", username="a", is_bot=False)
    _ = TelegramUser.objects.create(id=2, first_name="B", username="b", is_bot=False)
    accounts = [
        Account.objects.create(number="1", money_amount=Decimal(10), owner=first_user),
        Account.objects.create(number="2", money_amount=Decimal(10), owner=first_user),
    ]
    cards = [
        Card.objects.create(number="1", money_amount=Decimal(10), account=accounts[1]),
    ]
    return accounts, cards


@pytest.fixture(scope="function")
def users_with_operation_fixture():
    users = [
        TelegramUser.objects.create(id=1, first_name="A", username="a", is_bot=False),
        TelegramUser.objects.create(id=2, first_name="A", username="b", is_bot=False),
        TelegramUser.objects.create(id=3, first_name="A", username="c", is_bot=False),
    ]
    accounts = [
        Account.objects.create(number="1", money_amount=Decimal(10), owner=users[0]),
        Account.objects.create(number="2", money_amount=Decimal(10), owner=users[1]),
        Account.objects.create(number="3", money_amount=Decimal(10), owner=users[1]),
    ]
    operation = Operation.objects.create(
        sender=users[0],
        recipient=users[1],
        source=accounts[0],
        destination=accounts[1],
        money_amount=Decimal(2),
    )
    return users, accounts, operation
