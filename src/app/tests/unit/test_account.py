import contextlib

import pytest

from app.internal.models.account import Account, AccountAccessDeniedError, NotEnoughMoneyError
from app.internal.models.telegram_user import TelegramUser


class TestAccount:
    OWNER_ID = "329714238"

    def test_no_money(self):
        with pytest.raises(NotEnoughMoneyError):
            raise Account().get_not_enough_money_error()

    @pytest.mark.parametrize(
        "user_to_check,expectation",
        [
            (TelegramUser(id=OWNER_ID), contextlib.nullcontext()),
            (TelegramUser(id="123481234"), pytest.raises(AccountAccessDeniedError)),
            (TelegramUser(id=OWNER_ID, username="ab"), contextlib.nullcontext()),
        ],
    )
    def test_check_ownership(self, user_to_check, expectation):
        user = TelegramUser(id=self.OWNER_ID)
        account = Account(owner=user)
        with expectation:
            account.check_ownership(user_to_check)
