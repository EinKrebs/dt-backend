import pytest

from app.internal.models.telegram_user import TelegramUser
from app.internal.services.telegram_user_service import add_user, get_related_usernames, get_user, get_user_by_name

pytestmark = pytest.mark.django_db


@pytest.mark.django_db
class TestUserService:
    pytestmark = pytest.mark.django_db

    @pytest.mark.parametrize(
        "user",
        [
            (TelegramUser(id=1)),
            (TelegramUser(id=2, first_name="ab", is_bot=False)),
        ],
    )
    def test_add_user(self, user, user_fixture):
        add_user(user)

    @pytest.mark.parametrize(
        "id, expected",
        [
            (1, TelegramUser(id=1)),
            (2, None),
        ],
    )
    def test_get_user(self, id, expected, user_fixture):
        user = get_user(id)
        assert user == expected

    @pytest.mark.parametrize(
        "username, expected",
        [
            ("a", TelegramUser(id=1)),
            ("ab", None),
        ],
    )
    def test_get_user_by_name(self, username, expected, user_fixture):
        user = get_user_by_name(username)
        assert user == expected

    @pytest.mark.parametrize(
        "user, expected_senders, expected_receivers",
        [(TelegramUser(id=1), [], ["b"]), (TelegramUser(id=2), ["a"], []), (TelegramUser(id=3), [], [])],
    )
    def test_get_related_usernames(self, user, expected_senders, expected_receivers, users_with_operation_fixture):
        senders, receivers = get_related_usernames(user)
        assert expected_senders == senders
        assert expected_receivers == receivers
